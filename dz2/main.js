
let vegetable = ['Капуста', 'Репа', 'Редиска', 'Морковка'];

console.log(vegetable.join('|'));

let names = 'Вася;Петя;Вова;Олег;ВладиSlave';

let namesToArr = function (str) {
  return str.split(';')
}

console.log(namesToArr(names));


let hello2 = function (name) {
  return `Hello ${name}`
}

console.log(hello2('Test'));

let arrElToUpper = function (arr) {
  let resultArr = []
  arr.forEach(el => resultArr.push(el.toUpperCase()));
  return resultArr
}

console.log(arrElToUpper(['яблоко', 'ананас', 'груша']));


let addOneForAll = function (...numbers) {
  return numbers.map(el => el + 1)
}

console.log(addOneForAll(1, 3, 4, 5, 6));

let getSum = function (...numbers) {
  let sum = 0;
  numbers.forEach(el => sum += el);
  return sum;
}

console.log( getSum(1, 2, 3, 5, 6))

let checkNumbers = function (arr) {
  numberArr = []
  arr.forEach(el => {
    if (typeof el === 'number') {
      numberArr.push(el)
    }
  })
  return numberArr;
}
console.log(checkNumbers([1, 'hello', 2, 3, 4, '5', '6', 7, null]))

let arrayTesting = function (arr) {
  return arr.filter(el => el)
}

console.log(arrayTesting([0, false, null, 0]))
