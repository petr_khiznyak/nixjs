let nameTypeCheck = function (name) {
  if (typeof name !== 'string') {
    console.log('Name type error');
    return
  }
  console.log(`Hello ${name}`)
}


nameTypeCheck('Vasya')
nameTypeCheck(123)


console.log(typeof 1)
console.log(typeof '123')
console.log(typeof true)
console.log(typeof undefined)
console.log(typeof BigInt(10))
console.log(typeof Symbol('qwerty'))
console.log(typeof {})
console.log(typeof null )
console.log(typeof {NaN})
